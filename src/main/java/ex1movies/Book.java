package ex1movies;

import java.time.LocalDate;

public class Book extends Creation implements Comparable<Book>{
    String title;
    String author;
    LocalDate releaseDate;

    public Book(String title, String author, LocalDate releaseDate) {
        this.title = title;
        this.author = author;
        this.releaseDate = releaseDate;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getCreator() {
        return author;
    }

    @Override
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String getDescriptionData() {
        return this.title + " " + this.author + " " + this.releaseDate;
    }

    @Override
    public String toString() {
        return "Book" + "title:'" + title + '\'' + ", author:'" + author + '\'' + ", releaseDate:" + releaseDate;
    }

    @Override
    public int compareTo(Book book) {
        return this.title.compareTo(book.getTitle());
    }
}
