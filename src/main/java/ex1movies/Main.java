package ex1movies;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Movie littleWomen = new Movie("Little women","Greta Gerwig", LocalDate.of(2020,1,31));
        Movie shapeOfWater = new Movie("Shape of water","Guillermo del Toro", LocalDate.of(2017,8,31));
        Movie likeCrazy = new Movie("Like crazy","Drake Doremus", LocalDate.of(2011,10,28));

        List<Movie> movies = new ArrayList<Movie>();
        movies.add(littleWomen);
        movies.add(shapeOfWater);
        movies.add(likeCrazy);

        SearchEngine searchEngine = new SearchEngine(movies);
        System.out.println(searchEngine.printCreationInfo("Like crazy"));
        System.out.println(searchEngine.printCreationInfo("Holiday"));
//        System.out.println(searchEngine.printCreationInfoException("Shape of water"));
////        System.out.println(searchEngine.printCreationInfoException("Holiday"));
//        System.out.println(searchEngine.printCreationInfoOptional("Shape of water"));
//        System.out.println(searchEngine.printCreationInfoOptional("Holiday"));

        Book harryPotter = new Book ("Harry Potter", "J.K.Rowling", LocalDate.of(1997,6,26));
        Book millennium = new Book ("Millennium", "Stieg Larsson", LocalDate.of(2008,9,16));
        Book oldBook = new Book ("An old book", "Stieg Larsson", LocalDate.of(1995,9,16));

        List<Book> books = new ArrayList<Book>();
        books.add(harryPotter);
        books.add(millennium);
        books.add(oldBook);
        SearchEngine searchEngineBooks = new SearchEngine(books);
        System.out.println(searchEngineBooks.printCreationInfo("Harry Potter"));
        System.out.println(searchEngineBooks.printCreationInfo("Sherlock Holmes"));

//        System.out.println("====================");
//        System.out.println("List unsorted:");
//        movies.forEach(x -> System.out.println(x.getDescriptionData()));
//
//        System.out.println("====================");
//        //wyswietla toStringa, a jak go nie mamy to adresy czy jakies hashcody
//        books.forEach(System.out::println);
//
//        System.out.println("====================");
//        System.out.println("List sorted by release date:");
//        Collections.sort(movies, new MovieByReleaseDate());
//        movies.forEach(x -> System.out.println(x.getDescriptionData()));
//
//        System.out.println("====================");
//        System.out.println("List sorted by title:");
//        movies.sort(Comparator.comparing(Movie::getTitle));
//        movies.forEach(x -> System.out.println(x.getDescriptionData()));
//
//        System.out.println("====================");
//        System.out.println("List sorted natural:");
//        Collections.sort(movies);
//        movies.forEach(x -> System.out.println(x.getDescriptionData()));
//
//        System.out.println("====================");
//        System.out.println("List sorted by director and title:");
//        movies.sort(Comparator.comparing(Movie::getCreator).thenComparing(Movie::getTitle));
//        movies.forEach(x -> System.out.println(x.getDescriptionData()));
//
//        System.out.println("====================");
//        System.out.println("List sorted by author:");
//        books.sort(Comparator.comparing(Book::getCreator));
//        books.forEach(x -> System.out.println(x.getDescriptionData()));

        System.out.println("====================");
        System.out.println("List sorted by release date:");

        Collections.sort(books, new BooksByReleaseDate());
        books.forEach(x -> System.out.println(x.getDescriptionData()));

//        System.out.println("====================");
//        System.out.println("List sorted by author and release date:");
//        books.sort(Comparator.comparing(Book::getCreator).thenComparing(Book::getReleaseDate));
//        books.forEach(b -> System.out.println(b.getDescriptionData()));

        System.out.println("====================");
        System.out.println("List sorted natural:");
        Collections.sort(books);
        books.forEach(x -> System.out.println(x.getDescriptionData()));

    }
}
