package ex1movies;

import java.time.LocalDate;

public class Movie extends Creation implements Comparable<Movie>{
    String title;
    String director;
    LocalDate releaseDate;


    public Movie(String title, String director, LocalDate releaseDate) {
        this.title = title;
        this.director = director;
        this.releaseDate = releaseDate;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getCreator() {
        return director;
    }

    @Override
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    //nie robimy zwykłego toStringa, bo coś tam
    @Override
    public String getDescriptionData() {
        return this.title + " " + this.director + " " + this.releaseDate;
    }
//okreslamy naturalne porzadkowanie obiektow klasy
    @Override
    public int compareTo(Movie movie) {
        return this.title.compareTo(movie.getTitle());
    }
}
