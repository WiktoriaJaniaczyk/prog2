package ex1movies;

import java.util.Comparator;

public class MovieByReleaseDate implements Comparator<Movie> {
    @Override
    public int compare(Movie movie1, Movie movie2) {
        return movie1.getReleaseDate().compareTo(movie2.getReleaseDate());
    }
}
