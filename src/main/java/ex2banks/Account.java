package ex2banks;

public class Account {
    private String type;
    private double accountBalance;

    public Account(String type, int accountBalance) {
        this.type = type;
        this.accountBalance = accountBalance;
    }

    public String getType() {
        return type;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    //wpłata na rachunek
    /**
     * Obsluguje wplate na rachunek
     * @param amount
     */
    public void payIn(double amount){
        this.accountBalance += amount;
//        accountBalance += amount;
    }

    //wypłata z rachunku
    /**
     * Obsluguje wyplate z rachunku
     * @param amount
     */
    public void withdraw(double amount){
        if(amount > 0 && amount <= this.accountBalance) {
            this.accountBalance -= amount;
        }
        else {
            System.out.println("Not enough money");
        }
    }

}
