package ex2banks;

import java.util.List;

public class Bank<E> {
    String name;
    private List<Client> clients;
    private List<Account> accounts;

    public Bank(String name, List<Client> clients, List<Account> accounts) {
        this.name = name;
        this.clients = clients;
        this.accounts = accounts;
    }

    /**
     * Method adds new client to bank's client list.
     * @param client
     */
    public void addNewClient(Client client){
        this.clients.add(client);
    }

    /**
     * Method removes clients when they don't have any accounts.
     * @param client
     */
    public void removeClient(Client client){
        if(client.getAccounts().size()==0){
            clients.remove(client);
        }else {
            System.out.println("Cannot remove client. Existing accounts: " + client.getAccountsType(client.accounts));
        }
    }







}
