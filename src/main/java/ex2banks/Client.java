package ex2banks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Client {
    private String name;
    private String surname;
    private String pesel;
    private String uniqueClientNumber;
    Account account;
    List<Account> accounts = new ArrayList<Account>();

    public Client(String name, String surname, String pesel, String uniqueClientNumber, List<Account> accounts) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.uniqueClientNumber = uniqueClientNumber;
        this.accounts = accounts;
    }

    public String getOwner() {
        return name + " " + surname;
    }

    public String getPesel() {
        return pesel;
    }

    public String getUniqueClientNumber() {
        return uniqueClientNumber;
    }

    public Account getAccount() {
        return account;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public List<String>getAccountsType(List<Account> accounts){
        List<String> types = accounts.stream()
                .map(a -> a.getType()).collect(Collectors.toList());
        return types;

    }

    public List<Double> getAccountsBalance(List<Account> accounts){
        List<Double> balances = accounts.stream()
                .map(a->a.getAccountBalance()).collect(Collectors.toList());
        return balances;
    }

    public void removeAccount(Account account){
        if(account.getAccountBalance() <= 0){
            accounts.remove(account);
        }else {
            System.out.println("Cannot remove account. Account balance: " + account.getAccountBalance());
        }

    }
}
