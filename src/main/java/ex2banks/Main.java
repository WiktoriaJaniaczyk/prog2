package ex2banks;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        //zalozenie rachunku dla klienta
        Account account1 = new Account("account name", 2000);
        Account account2 = new Account("account name2", 100);
        Account account3 = new Account("account name3", 2000);
        Account account4 = new Account("account name4", 100);

        List<Account> client1Accounts = new ArrayList<Account>();
        client1Accounts.add(account1);
        client1Accounts.add(account2);

        List<Account> bankAccounts = new ArrayList<>();
        bankAccounts.addAll(client1Accounts);

        List<Account> client2Accounts = new ArrayList<>();
        client2Accounts.add(account3);
        client2Accounts.add(account4);

        //List<Account> client3Accounts = new ArrayList<>();

        bankAccounts.addAll(client2Accounts);


        //zalozenie klienta
        Client client1 = new Client("Connor","Walsh","92101234567","1111",client1Accounts);
        Client client2 = new Client("Michaela","Pratt","94042023456","1234",client2Accounts);

        List<Client> santanderClients = new ArrayList<Client>();
        santanderClients.add(client1);
        santanderClients.add(client2);

        Bank santanderBank = new Bank("Santander",santanderClients,bankAccounts);

        System.out.println(account1.getAccountBalance());
        //wplata na rachunek
        account1.payIn(200);
        System.out.println(account1.getAccountBalance());
        //wyplata z rachunku
        account1.withdraw(300);
        System.out.println(account1.getAccountBalance());

        //usuniecie rachunku dla klienta
        client2.removeAccount(account3);
        //usuniecie klienta
        santanderBank.removeClient(client2);

        //lista klientow banku z lista rachunkow
        printClientsListWithTheirAccounts(santanderClients);
        System.out.println("-----------");
        //lista rachunkow klienta z saldem
        printClientAccountsWithBalance(client1);

    }

    private static void printClientAccountsWithBalance(Client client1) {
        System.out.println(client1.getOwner() + " accounts with balance:");
        client1.getAccounts().forEach(x-> System.out.println(x.getType()+" "+ x.getAccountBalance()));
    }

    private static void printClientsListWithTheirAccounts(List<Client> santanderClients) {
        santanderClients.forEach(x -> System.out.println(x.getOwner() + " " + x.getAccountsType(x.accounts)));
    }
}
