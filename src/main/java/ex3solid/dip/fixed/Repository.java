package ex3solid.dip.fixed;

public interface Repository {

    void saveTask(String task);

    void deleteTask(int taskId);
}
