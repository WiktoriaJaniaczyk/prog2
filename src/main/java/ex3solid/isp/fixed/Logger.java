package ex3solid.isp.fixed;

public interface Logger {

    void writeMessage(String message);
}
