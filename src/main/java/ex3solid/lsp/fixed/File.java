package ex3solid.lsp.fixed;

public class File implements FileReadable,FileWritable{
    @Override
    public byte[] read() {
        return new byte[0];
    }

    @Override
    public byte[] write() {
        return new byte[0];
    }
}
