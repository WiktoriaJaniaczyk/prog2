package ex3solid.lsp.fixed;

public interface FileReadable {

    byte[] read();
}
