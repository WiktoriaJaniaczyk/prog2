package ex3solid.lsp.fixed;

public interface FileWritable {

    byte[] write();
}
