package ex3solid.ocp;

public enum LogDestination {
    CONSOLE,
    DB
}
