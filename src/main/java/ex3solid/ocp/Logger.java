package ex3solid.ocp;

public class Logger {

    private LogDestination destination;

    public Logger(LogDestination destination) {
        this.destination = destination;
    }

    public void log(String message){
        switch(destination){
            case CONSOLE:
                System.out.println("Printing to console: " + message);
                break;
            case DB:
                System.out.println("Printing to DB: " + message);
                break;
            default:
                throw new IllegalArgumentException("Unsupported logging type!");
        }
    }
}
