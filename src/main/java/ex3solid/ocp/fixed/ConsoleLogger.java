package ex3solid.ocp.fixed;

public class ConsoleLogger implements MessageLogger{
    @Override
    public void log(String message) throws Exception {
//write message to console
        System.out.println("Write message to console.");
    }
}
