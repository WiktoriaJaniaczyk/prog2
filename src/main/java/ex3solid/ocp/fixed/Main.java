package ex3solid.ocp.fixed;

public class Main {
    public static void main(String[] args) throws Exception {

        MessageLogger dbMessageLogger = new DatabaseLogger();
        MessageLogger consoleMessageLogger = new ConsoleLogger();


        Logger dblogger = new Logger(dbMessageLogger);
        dblogger.log("dblogger");
        Logger clogger = new Logger(consoleMessageLogger);
        clogger.log("hello");

    }
}
