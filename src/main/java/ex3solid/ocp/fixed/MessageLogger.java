package ex3solid.ocp.fixed;

public interface MessageLogger {

    void log(String message) throws Exception;

}
