package ex3solid.srp.fixed;

public class DrivingLicence {

    private static final int DRIVING_LICENCE_REQUIRED_AGE = 18;
    private static final int DRIVING_LICENCE_REQUIRED_AGE_WITH_MENTOR = 16;

    public static boolean canGetDrivingLicense(Person person){
        //ekstraktować 18 poza klasę i wczytywać jako konfigurację
        return person.getAge() > DRIVING_LICENCE_REQUIRED_AGE;
    }
}
