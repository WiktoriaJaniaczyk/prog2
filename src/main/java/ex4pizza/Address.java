package ex4pizza;

public class Address {

    private String street;
    private String streetNumber;
    private String localNumber;

    public Address(String street, String streetNumber) {
        this.street = street;
        this.streetNumber = streetNumber;
    }

    public Address(String street, String streetNumber, String localNumber) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.localNumber = localNumber;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getLocalNumber() {
        return localNumber;
    }

    public String getAddress(){
        if(localNumber==null){
            return street + " " + streetNumber;
        }
        return street + " " + streetNumber + "/" + localNumber;
    }
}
