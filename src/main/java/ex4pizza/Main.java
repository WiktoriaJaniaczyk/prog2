package ex4pizza;

public class Main {

    public static void main(String[] args) {


        Pizza margherita = new Pizza("Margherita");
        Pizza pizzaWithHamAndCheese = new Pizza("Pizza with ham and cheese");
        Pizza pizzaWithMushroomsAndCheese = new Pizza("Pizza with mushrooms and cheese");


        Address address1 = new Address(  "Baker Street", "221","b");
        Address address2 = new Address(  "Baker Street", "221");
        User sherlock = new User("sherlock.holmes@gmail.com", address1);

//        PizzaOrderService pizzaOrderService = new PizzaOrderService();
//        pizzaOrderService.order(sherlock, address1,margherita);

        PizzaOrderServiceNew pizzaOrderServiceNew = new PizzaOrderServiceNew();
        pizzaOrderServiceNew.order(sherlock,address1,margherita);
        pizzaOrderServiceNew.order(sherlock,address2,margherita);







    }
}
