package ex4pizza;

public interface OrderService {

    void order(User user, Address address, Product product);
}
