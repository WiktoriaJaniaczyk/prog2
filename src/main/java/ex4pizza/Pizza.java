package ex4pizza;

public class Pizza implements Product{
    String name;

    public Pizza(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
