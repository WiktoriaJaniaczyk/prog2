package ex4pizza;

public class PizzaOrderService {

    public void order(User user, Address address, Product product){
        System.out.println("Request send from: " + user.getEmail());
        System.out.println("by " + user.getEmail());
        System.out.println("Ordering pizza " + product.getName()+ " "+
                "on address " + address.getStreet() + " " + address.getStreetNumber() +
                ((address.getLocalNumber() != null) ? ("/" + address.getLocalNumber() + " ") : " "));

    }
}
