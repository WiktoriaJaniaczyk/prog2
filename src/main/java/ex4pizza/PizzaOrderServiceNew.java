package ex4pizza;

public class PizzaOrderServiceNew implements OrderService {
    @Override
    public void order(User user, Address address, Product product) {
        System.out.println("Request send from: " + user.getEmail());
        System.out.println("Ordering pizza " + product.getName()+ " "+
                "on address "  + address.getAddress());
    }
}
