package ex4pizza;

public class User {
    private String email;
    private Address address;

    public User(String email, Address address) {
        this.email = email;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

}
