package patterns.behavioral.visitor;

public interface ItemVisitor {
    //na kazdy element musimy miec osobna metode visit
    int visit(Item1 item1);

    int visit(Item2 item2);
}
