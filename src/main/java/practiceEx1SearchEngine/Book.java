package practiceEx1SearchEngine;

import java.time.LocalDate;
import java.util.Comparator;

public class Book extends Creation implements Comparable<Book> {
    String title;
    String author;
    LocalDate releaseDate;

    public Book(String title, String author, LocalDate releaseDate) {
        this.title = title;
        this.author = author;
        this.releaseDate = releaseDate;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getCreator() {
        return author;
    }

    @Override
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String getDescription() {
        return this.title + " " + this.author + " " + this.releaseDate;
    }

    @Override
    public int compareTo(Book book) {
        return this.getTitle().compareTo(book.getTitle());
    }

    @Override
    public String toString() {
        return "* " + title + " " + author + " " + releaseDate;
    }
}
