package practiceEx1SearchEngine;

public class CreationNotFoundException extends Exception{
    public CreationNotFoundException() {
        super("Creation not found exception");
    }
}
