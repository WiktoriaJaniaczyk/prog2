package practiceEx1SearchEngine;

import java.time.LocalDate;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Movie littleWomen = new Movie("Little women","Greta Gerwig", LocalDate.of(2020,1,31));
        Movie shapeOfWater = new Movie("Shape of water","Guillermo del Toro", LocalDate.of(2017,8,31));
        Movie likeCrazy = new Movie("Like crazy","Drake Doremus", LocalDate.of(2011,10,28));

        List<Movie> myMovies = new ArrayList<>();
        myMovies.add(littleWomen);
        myMovies.add(shapeOfWater);
        myMovies.add(likeCrazy);

        SearchEngine searchMyMovieEngine = new SearchEngine(myMovies);
//        System.out.println(searchMyMovieEngine.printMovieInfo("Like crazy"));
//        System.out.println(searchMyMovieEngine.printMovieInfo("Like you"));
//        System.out.println(searchMyMovieEngine.printDirectorInfo("Greta Gerwig"));
//        System.out.println(searchMyMovieEngine.printDirectorInfo("Roman Polanski"));
//        System.out.println(searchMyMovieEngine.printMovieInfoOptional("Like crazy"));
//        System.out.println(searchMyMovieEngine.printMovieInfoOptional("Like y"));
//        System.out.println(searchMyMovieEngine.printCreationInfoException("Like crazy"));
//        System.out.println(searchMyMovieEngine.printCreationInfoException("Like y"));

        Book harryPotter = new Book("Harry Potter", "J.K.Rowling",LocalDate.of(1997,7,28));
        Book harryPotter2 = new Book("Harry Potter 2", "J.K.Rowling",LocalDate.of(1997,7,28));
        Book millennium = new Book("Millennium", "Stieg Larsson", LocalDate.of(2008,9,16));
        Book sherlockHolmes = new Book("Sherlock Holmes", "A.C. Doyle",LocalDate.of(1992,7,28));
        List<Book> myBooks = new ArrayList<>();
        myBooks.add(sherlockHolmes);
        myBooks.add(harryPotter);
        myBooks.add(millennium);
        myBooks.add(harryPotter2);
        SearchEngine searchMyBooksEngine = new SearchEngine(myBooks);
//        System.out.println(searchMyBooksEngine.printCreationInfo("Sherlock Holmes"));
//        System.out.println(searchMyBooksEngine.printCreationInfo("Little women"));

        //#### SORTOWANIE #####

        printBooksUnsorted(myBooks);
//        printBooksToString(myBooks);
//        printBooksInAlphabeticalOrderByTitles(myBooks);
//        printBooksInAlphabeticalOrderByAuthorAndTitle(myBooks);
//        printBooksInAlphabeticalOrderByAuthor(myBooks);
//        printBooksInNaturalOrder(myBooks);
        printBooksByReleaseDate(myBooks);
    }

    private static void printBooksByReleaseDate(List<Book> myBooks) {
        System.out.println("**************************");
        System.out.println("List sorted by release date: ");
        Collections.sort(myBooks,new BooksByReleaseDate());
        myBooks.forEach(b-> System.out.println(b.getDescription()));
    }


    private static void printBooksInNaturalOrder(List<Book> myBooks) {
        System.out.println("**************************");
        //natural order here means by title
        System.out.println("List sorted in natural order");
        //you should define in your class how to compare
        Collections.sort(myBooks);
        myBooks.forEach(b-> System.out.println(b.getDescription()));
    }

    private static void printBooksInAlphabeticalOrderByAuthorAndTitle(List<Book> myBooks) {
        System.out.println("**************************");
        System.out.println("List sorted by author and title");
        myBooks.sort(Comparator.comparing(Book::getCreator).thenComparing(Book::getTitle));
        myBooks.forEach(b-> System.out.println(b.getDescription()));
    }


    private static void printBooksInAlphabeticalOrderByAuthor(List<Book> myBooks) {
        System.out.println("**************************");
        myBooks.sort(Comparator.comparing(Book::getCreator));
        myBooks.forEach(b-> System.out.println(b.getDescription()));
    }

    private static void printBooksInAlphabeticalOrderByTitles(List<Book> myBooks) {
        System.out.println("**************************");
        myBooks.sort(Comparator.comparing(Book::getTitle));
        myBooks.forEach(b-> System.out.println(b.getDescription()));
    }

    private static void printBooksToString(List<Book> myBooks) {
        System.out.println("**************************");
        System.out.println("List of books using toString (or hashcodes): ");
        myBooks.forEach(System.out::println);
    }

    private static void printBooksUnsorted(List<Book> myBooks) {
        //wyswietla dane na temat kazdej pozycji w myBooks
        System.out.println("**************************");
        System.out.println("List unsorted:");
        myBooks.forEach(b -> System.out.println(b.getDescription()));
    }


}
