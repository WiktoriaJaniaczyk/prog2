package practiceEx1SearchEngine;

import java.time.LocalDate;

public class Movie extends Creation {

    String title;
    String director;
    LocalDate releaseDate;

    public Movie(String title, String director, LocalDate releaseDate) {
        this.title = title;
        this.director = director;
        this.releaseDate = releaseDate;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getCreator() {
        return director;
    }

    @Override
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String getDescription() {
        return this.title + " " + this.director + " " + this.releaseDate;
    }
}
